<b>ArpSpoof</b> is a module created by AnguisCaptor to start and stop arpspoof<br>
ArpSpoof redirects packets from a target host (or all hosts) on the LAN intended for another host on the LAN by forging ARP replies. This is an extremely effective way of sniffing traffic on a switch.
<br><br>
