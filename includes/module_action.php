<? 
/*
    Copyright (C) 2013-2015 xtr4nge [_AT_] gmail.com
	Module ArpSpoof created by @AnguisCaptor

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
?>
<?

include "../../../config/config.php";
include "../_info_.php";
include "../../../login_check.php";
include "../../../functions.php";

include "options_config.php";

// Checking POST & GET variables...
if ($regex == 1) {
    regex_standard($_GET["service"], "../msg.php", $regex_extra);
    regex_standard($_GET["action"], "../msg.php", $regex_extra);
    regex_standard($_GET["page"], "../msg.php", $regex_extra);
    regex_standard($_GET["install"], "../msg.php", $regex_extra);
}

$service = $_GET['service'];
$action = $_GET['action'];
$page = $_GET['page'];
$install = $_GET['install'];

if($service != "") {
    
    if ($action == "start") {
		
		$exec = "$bin_arpspoof -i $arpspoof_interface -t $arpspoof_routerip $arpspoof_targetip > /dev/null 2>&1 &";
		exec_fruitywifi($exec);
		
		$exec = "$bin_arpspoof -i $arpspoof_interface -t $arpspoof_targetip $arpspoof_routerip > /dev/null 2>&1 &";
		exec_fruitywifi($exec);
        
    } else if($action == "stop") {
        // STOP MODULE
        $exec = "$bin_killall $bin_arpspoof";
        exec_fruitywifi($exec);
    }
}

if ($install == "install_$mod_name") {

    $exec = "chmod 755 install.sh";
    exec_fruitywifi($exec);

    $exec = "$bin_sudo ./install.sh";
    exec_fruitywifi($exec);

    header('Location: ../../install.php?module='.$mod_name);
    exit;
}

if ($page == "status") {
    header('Location: ../../../action.php');
} else if($service != "" && $action == "start") {
	header('Location: ../../action.php?page='.$mod_name);
} else {
	//it ussually takes ~5 seconds for arpspoof to re-arp the targets and exit
    header('Location: ../../action.php?page='.$mod_name.'&wait=5');
}

?>
