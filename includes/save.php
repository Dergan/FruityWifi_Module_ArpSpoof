<? 
/*
	Copyright (C) 2013-2015 xtr4nge [_AT_] gmail.com
	Module ArpSpoof created by @AnguisCaptor

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 
?>
<?

include "../../../config/config.php";
include "../_info_.php";
include "../../../login_check.php";
include "../../../functions.php";

include "options_config.php";

// Checking POST & GET variables...
if ($regex == 1) {
	regex_standard($_POST['type'], "../../../msg.php", $regex_extra);
	regex_standard($_POST['arpspoof_interface'], "../../../msg.php", $regex_extra);
	regex_standard($_POST['arpspoof_routerip'], "../../../msg.php", $regex_extra);
	regex_standard($_POST['arpspoof_targetip'], "../../../msg.php", $regex_extra);
}

$type = $_POST['type'];
$arpspoof_interface = $_POST["arpspoof_interface"];
$arpspoof_routerip = $_POST["arpspoof_routerip"];
$arpspoof_targetip = $_POST["arpspoof_targetip"];

if ($type == "settings") {

    $exec = "/bin/sed -i 's/arpspoof_interface.*/arpspoof_interface = \\\"".$arpspoof_interface."\\\";/g' options_config.php";
    $output = exec_fruitywifi($exec);

	$exec = "/bin/sed -i 's/arpspoof_routerip.*/arpspoof_routerip = \\\"".$arpspoof_routerip."\\\";/g' options_config.php";
    $output = exec_fruitywifi($exec);

	$exec = "/bin/sed -i 's/arpspoof_targetip.*/arpspoof_targetip = \\\"".$arpspoof_targetip."\\\";/g' options_config.php";
    $output = exec_fruitywifi($exec);
	
    header('Location: ../index.php?tab=0');
    exit;

}

header('Location: ../index.php');

?>
